use mysql;
grant all privileges on *.* to 'root'@'%' identified by '123456';
flush privileges;

create database mytest;

use mytest;

create table user (
    id int auto_increment primary key,
    username varchar(64) unique not null,
    email varchar(120) unique not null,
    password_hash varchar(128) not null,
    avatar varchar(128) not null;
)
insert into user values(1, "zhangsan", "zhangsan123@qq.com", "password", "avaterpath"); 
